#ifndef EPIC_SERIAL_H
#define EPIC_SERIAL_H

#define SERIAL_READ_BUFFER_SIZE 128

void vSerialTask(void*pvParameters);

#endif /* EPIC_SERIAL_H */
