#include "py/mpconfig.h"

/* leds */
Q(leds)

/* utime */
Q(utime)
Q(sleep)
Q(sleep_ms)
Q(sleep_us)
Q(ticks_ms)
Q(ticks_us)
Q(ticks_cpu)
Q(ticks_add)
Q(ticks_diff)
