var NAVTREE =
[
  [ "MAX32665 SDK Documentation", "index.html", [
    [ "License Agreement", "index.html#license", null ],
    [ "API Overview and Usage", "md_API_Overview_and_Usage.html", [
      [ "Introduction", "md_API_Overview_and_Usage.html#introPage", null ],
      [ "CMSIS", "md_API_Overview_and_Usage.html#cmsisPage", [
        [ "Register-level Header Files", "md_API_Overview_and_Usage.html#regHeadersPage", null ],
        [ "Startup Code", "md_API_Overview_and_Usage.html#startupCodePage", null ],
        [ "Peripheral Drivers", "md_API_Overview_and_Usage.html#periphDriverPage", null ],
        [ "System Layer", "md_API_Overview_and_Usage.html#sysLayerPage", null ],
        [ "Board Support Package", "md_API_Overview_and_Usage.html#bspPage", null ]
      ] ],
      [ "Synchronous vs. Asynchronous APIs", "md_API_Overview_and_Usage.html#asyncAPIPage", null ],
      [ "Low-power API Considerations", "md_API_Overview_and_Usage.html#lpPage", null ],
      [ "Debug Assertions", "md_API_Overview_and_Usage.html#debugAssertPage", null ],
      [ "Makefile Structure and Building Projects", "md_API_Overview_and_Usage.html#makefilePage", null ]
    ] ],
    [ "Modules", "modules.html", "modules" ],
    [ "Data Structures", "annotated.html", [
      [ "Data Structures", "annotated.html", "annotated_dup" ],
      [ "Data Structure Index", "classes.html", null ],
      [ "Data Fields", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Variables", "functions_vars.html", "functions_vars" ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html",
"group__DMA__CFG.html#ga9127fddb84cfa1694e143c092a147cdc",
"group__FLC__CN.html#gafbed0de29b325de1fedd4a0809da2cea",
"group__HTMR__RAS.html#ga9cb0d586b37fdf1e0bf6a8d94ef8513c",
"group__I2C__TX__CTRL0.html#ga90b0482fe14bbeae88f69d1fa8e8cf16",
"group__PWRSEQ__LPCN.html#ga66ecb773cefefc1c878762e1983f18d4",
"group__RPU__DMA0.html#ga44be5d94765c607470bc69558159bf84",
"group__RPU__I2C2.html#ga36f610e4b6698657571271a48b5f3f35",
"group__RPU__Register__Offsets.html#ga6be2aeefa0e54c4269d488de15b342dc",
"group__RPU__SPIXIPMFIFO.html#ga26f1290d687d0c6e89606e0712f9433d",
"group__RPU__SRAM5.html#gad07972bac217d97c026818302b2b3379",
"group__RPU__USBHS.html#ga6ad4446805d3726c276432b74c2caabd",
"group__SDHC__CFG__1.html#ga26c6e323ecd6c1ef6409bf039caba2fd",
"group__SDHC__INT__STAT.html#gade91f6179f373891d4999a7d6026563a",
"group__SPI17Y__CTRL2.html#ga7fdfd9a02c07700a4545f67ac22572d4",
"group__SPIXFC__GEN__CTRL.html#ga906892eca34dbd22a18324838715335d",
"group__SPIXR__CTRL3.html#ga72a1096a5051e718e0a447645019af78",
"group__TMR__CN.html#ga7d1a282f0c3d74f7147bfd311e2fd11e",
"group__TPU__Register__Offsets.html#ga71b00749b9e113af749cd01db398c999",
"group__WDT__CTRL.html#ga7589c42720c3eb2be8f78aa101c52177",
"group__gpio__pin.html#ga347ca13449e59d7377624b64bc42d464",
"group__pwrseq.html#gab7a732cd0441022435a1733548d0f5d0",
"group__uart.html#gac25fcf192c6342911a61d4b45c0e5063",
"structmxc__rpu__regs__t.html#a0f8e7eede4657507a92895cfebf19e4c",
"structmxc__uart__regs__t.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';